#!/usr/bin/env bash

pushd "$(dirname "$0")" > /dev/null

set -e

mkdir -p out

debug=$([ "x$2" = "xdebug" ] && echo "true" || echo "false")
if [ "$debug" = true ]; then
  echo "Debug mode enabled"
fi

base_name="stricthtml"
out_base_name="out/${base_name}_${1}"

case "$1" in
c)
  gcc -std=c11 -O2 $([ "$debug" = true ] && echo "-DDEBUG_LOG" || echo "") -Wall -Werror \
    "src/c/$base_name.c" -o "$out_base_name"
  ;;

em)
  emcc "$([ "$debug" = true ] && echo "" || echo "-O2")" -Wall -Werror \
    "$([ "$debug" = true ] && echo "-DDEBUG_LOG" || echo "")" \
    -s ASSERTIONS="$([ "$debug" = true ] && echo "2" || echo "0")" \
    -s RESERVED_FUNCTION_POINTERS=1 \
    --post-js "rules/compiled/rules.js" \
    --post-js "src/em/$base_name.em.post.js" \
    "src/em/$base_name.em.c" -o "$out_base_name.js"

  node compile.em.js "$([ "$debug" = true ] && echo "debug" || echo "")"
  ;;

*)
  echo "Unknown file type"
  exit 1
  ;;
esac

popd > /dev/null

exit 0
