#!/usr/bin/env bash

shopt -s nullglob

echoerr() {
  cat <<< "$@" 1>&2
}

this_dir="$(realpath "$(dirname "$0")")"
src="$this_dir/../../src";
out="$this_dir/../../../out";

pushd "$src" > /dev/null

cd pass

for test in *.txt; do
  echo "Running pass test $test"

  ${out}/stricthtml_c < "$test" > /dev/null
  errno=$?

  if [ "$errno" != "0" ]; then
    echoerr "Pass test $test failed"
    exit 1;
  fi
done

cd ..
cd fail

for test in *.txt; do
  echo "Running fail test $test"
  expected_error="$(echo "$test" | grep -o -E '^[0-9]+')"

  if [[ -z "${expected_error// }" ]]; then
    echoerr "Invalid fail test $test"
    exit 2
  fi

  ${out}/stricthtml_c < "$test" &> /dev/null
  errno=$?

  if [ "$errno" == "0" ]; then
    echoerr "Fail test $test passed; expected error $expected_error"
    exit 1
  fi

  if [ "$errno" != "$expected_error" ]; then
    echoerr "Fail test $test got error "$errno"; expected error $expected_error"
    exit 1
  fi
done

cd ..
cd mirror

for test in *.txt; do
  echo "Running mirror test $test"
  md5_exp="$(md5sum "$test" | awk '{print $1}')"

  # Can't directly store output as some tests output NULL chars, so write to file
  tmp_file="$(mktemp)"

  ${out}/stricthtml_c < "$test" > "$tmp_file"
  errno=$?

  if [ "$errno" != "0" ]; then
    echoerr "Mirror test $test failed"
    rm "$tmp_file"
    exit 1;
  fi

  md5_res="$(md5sum "$tmp_file" | awk '{print $1}')"
  if [ "$md5_exp" != "$md5_res" ]; then
    echoerr "Mirror test $test failed due to MD5 mismatch"
    rm "$tmp_file"
    exit 1
  fi

  rm "$tmp_file"
done

cd ..

echo "All tests complete"

popd > /dev/null

exit 0
