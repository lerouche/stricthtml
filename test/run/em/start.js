#!/usr/bin/env node

"use strict";

const fs = require('fs');

const SRC_DIR = __dirname + "/../../src";
const OUT_DIR = __dirname + "/../../../out";

const stricthtml_em = require(OUT_DIR + "/stricthtml_em.js");

for (let pass_test of fs.readdirSync(SRC_DIR + '/pass')) {
  console.log(`Running pass test ${pass_test}...`);

  let code = fs.readFileSync(SRC_DIR + '/pass/' + pass_test, 'utf8');

  stricthtml_em.parse(code, function (err, doc) {
    if (err) {
      console.error(`Pass test ${pass_test} failed\n${err.message}`);
      process.exit(1);
    }
  });
}

for (let fail_test of fs.readdirSync(SRC_DIR + '/fail')) {
  console.log(`Running fail test ${fail_test}...`);
  if (!/^[0-9]+/.test(fail_test)) {
    throw new Error(`Invalid fail test ${fail_test}`);
  }
  let expected_fail = /(^[0-9]+)/.exec(fail_test)[1] | 0;

  let code = fs.readFileSync(SRC_DIR + '/fail/' + fail_test, 'utf8');

  stricthtml_em.parse(code, function (err, doc) {
    if (!err) {
      console.error(`Fail test ${fail_test} passed, expected error ${expected_fail}`);
      process.exit(1);
    }

    if (err.shErrNo !== expected_fail) {
      console.error(`Fail test ${fail_test} failed with error ${err.shErrNo}, expected error ${expected_fail}`);
      process.exit(1);
    }
  });
}

for (let mirror_test of fs.readdirSync(SRC_DIR + '/mirror')) {
  console.log(`Running mirror test ${mirror_test}...`);

  let original = fs.readFileSync(SRC_DIR + '/mirror/' + mirror_test, 'utf8');

  stricthtml_em.parse(original, function (err, doc) {
    if (err) {
      console.error(`Mirror test ${mirror_test} failed\n${err.message}`);
      process.exit(1);
    }

    if (stricthtml_em.stringify(doc) !== original) {
      console.error(`Mirror test ${mirror_test} failed due to mismatch`);
      process.exit(1);
    }
  });
}

console.log(`All tests complete`);
