#ifndef _HDR_STRICTHTML_STRING
#define _HDR_STRICTHTML_STRING

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#define STRING_INIT_SIZE 50
#define STRING_REALLOC_RATE 1.5

#define SIZE_CHAR sizeof(char)

typedef struct {
  char *buffer;
  uint32_t length;
  uint32_t bufferSize;
} String;

String *stringFromCharArray(uint32_t length, char *buffer, uint32_t buffer_size) {
  String *str = calloc(1, sizeof(String));
  str->length = length;
  str->bufferSize = buffer_size;
  str->buffer = buffer;
  return str;
}

String *newString() {
  // Always leave room for null-terminator for safety and compatibility (e.g. printing)
  char *buffer = calloc(1 + STRING_INIT_SIZE, SIZE_CHAR);
  return stringFromCharArray(0, buffer, STRING_INIT_SIZE);
}

void deleteString(String *string) {
  // WARNING: buffer will be free'd even if it wasn't created by newString
  free(string->buffer);
  free(string);
}

// NULL-char-safe printing
void printString(String *string) {
  for (uint32_t i = 0; i < string->length; i++) {
    printf("%c", string->buffer[i]);
  }
}

void appendToString(String *str, char c) {
  uint32_t length = str->length;
  uint32_t buffer_size = str->bufferSize;
  if (length == buffer_size) {
    uint32_t new_buffer_size = buffer_size * STRING_REALLOC_RATE;
    // Always leave room for null-terminator for safety and compatibility (e.g. printing)
    str->buffer = realloc(str->buffer, SIZE_CHAR * (1 + new_buffer_size));
    str->bufferSize = new_buffer_size;
  }
  str->buffer[length] = c;
  str->length++;
}

void removeLastCharFromString(String *str) {
  uint32_t length = str->length;
  if (length > 0) {
    str->buffer[length - 1] = '\0';
    str->length--;
  }
}

char getFromString(String *str, uint32_t idx) {
  if (idx >= str->length) {
    return '\0';
  }

  return str->buffer[idx];
}

// WARNING: <to_match> should not contain NULL characters
int stringMatches(String *str, uint32_t start, const char *to_match) {
  for (uint32_t i = 0; i < strlen(to_match); i++) {
    uint32_t offset = start + i;
    if (offset >= str->length) {
      return 0;
    }
    if (str->buffer[offset] != to_match[i]) {
      return 0;
    }
  }
  return 1;
}

int equalStrings(String *a, String *b) {
  if (a->length == b->length &&
  strncmp(a->buffer, b->buffer, a->length) == 0) {
    return 1;
  }
  return 0;
}

// WARNING: <b> should not contain NULL characters
int equalCharArrays(String *a, const char *b) {
  if (a->length == strlen(b) &&
  strncmp(a->buffer, b, a->length) == 0) {
    return 1;
  }
  return 0;
}

#endif // _HDR_STRICTHTML_STRING
