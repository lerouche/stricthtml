#ifndef _HDR_STRICTHTML_CODE
#define _HDR_STRICTHTML_CODE

#include "./String.h"

typedef struct {
  String *string;

  uint32_t nextPos;
  uint32_t curLine;
  uint32_t curCol;

  int lastCharWasCR;
} Code;

Code *newCode(String *code_str) {
  Code *code = calloc(1, sizeof(Code));
  code->string = code_str;
  code->nextPos = 0;
  code->curLine = 1;
  code->curCol = 0;
  return code;
}

char codePeek(Code *code, uint32_t offset) {
  if (offset == 0 && code->nextPos == 0) {
    return '\0';
  }

  uint32_t pos = code->nextPos + offset - 1;
  if (pos >= code->string->length) {
    return '\0';
  }
  return getFromString(code->string, pos);
}

char codeForward(Code *code) {
  char c = getFromString(code->string, code->nextPos);

  switch (c) {
  case '\r':
    code->lastCharWasCR = 1;
    code->curLine++;
    code->curCol = 0;
    break;

  case '\n':
    if (!code->lastCharWasCR) {
      code->curLine++;
      code->curCol = 0;
    }

    code->lastCharWasCR = 0;
    break;

  default:
    code->curCol++;
    code->lastCharWasCR = 0;
  }

  code->nextPos++;

  return c;
}

int codeNextMatches(Code *code, uint32_t offset, const char *to_match) {
  // This checks if <to_match> is the same as the next sequence of characters,
  // starting at nextPos
  return stringMatches(code->string, code->nextPos + offset, to_match);
}

int codeAtEnd(Code *code) {
  // Use >= just in case
  return code->nextPos >= code->string->length;
}

#endif // _HDR_STRICTHTML_CODE
