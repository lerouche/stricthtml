#ifndef _HDR_STRICTHTML_LIST
#define _HDR_STRICTHTML_LIST

#include <stdlib.h>
#include <inttypes.h>

#define LIST_INIT_LENGTH 10
#define LIST_REALLOC_RATE 1.5

typedef struct list_st List;

struct list_st {
  void **buffer;
  uint32_t length;
  uint32_t bufferSize;
};

List *newList() {
  void **buffer = calloc(LIST_INIT_LENGTH, sizeof(void *));
  List *list = calloc(1, sizeof(List));
  list->length = 0;
  list->bufferSize = LIST_INIT_LENGTH;
  list->buffer = buffer;
  return list;
}

void deleteList(List *list) {
  free(list->buffer);
  free(list);
}

void pushToList(List *list, void *item) {
  uint32_t length = list->length;
  uint32_t buffer_size = list->bufferSize;
  if (length == buffer_size) {
    uint32_t new_buffer_size = buffer_size * LIST_REALLOC_RATE;
    list->buffer = realloc(list->buffer,
      sizeof(void *) * new_buffer_size);
    list->bufferSize = new_buffer_size;
  }
  list->buffer[length] = item;
  list->length++;
}

void *getFromList(List *list, uint32_t idx) {
  if (idx >= list->length) {
    return NULL;
  }

  return list->buffer[idx];
}

#endif // _HDR_STRICTHTML_LIST
