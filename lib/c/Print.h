#ifndef _HDR_STRICTHTML_PRINT
#define _HDR_STRICTHTML_PRINT

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "./String.h"

void debug(int level, const char *msg, ...) {
#ifdef DEBUG_LOG
  // WARNING: <msg> should not contain NULL characters
  char *format = malloc(strlen(msg) + sizeof(char) * 30);
  sprintf(format, "[DBG%d] %s", level, msg);

  va_list args;
  va_start(args, msg);
  vfprintf(stderr, format, args);
  va_end(args);
#endif // DEBUG_LOG
}

#endif // _HDR_STRICTHTML_PRINT
