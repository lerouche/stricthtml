#ifndef _HDR_STRICTHTML_REPRESENT
#define _HDR_STRICTHTML_REPRESENT

/*

  NOTE: This library is not used by the emscripten version.
  That version has a JS copy of the following code.
  Make sure to reflect any changes there too.

 */

#include <stdio.h>

#include "./DOM.h"
#include "../../rules/compiled/rules.h"

void domRepNode(Node *node);

void domRepComment(Node *n_comment) {
  printf("<!--");
  printString(n_comment->value);
  printf("-->");
}

void domRepDoctype(Node *n_doctype) {
  printf("<!");
  printString(n_doctype->value);
  printf(">");
}

void domRepText(Node *n_text) {
  printString(n_text->value);
}

void domRepElement(Node *n_element) {
  printf("<");
  printString(n_element->tagName);
  for (uint32_t i = 0; i < n_element->attrs->length; i++) {
    Attr *attr = (Attr *) getFromList(n_element->attrs, i);
    printf(" ");
    printString(attr->name);
    printf("=\"");
    printString(attr->value);
    printf("\"");
  }
  printf(">");
  for (uint32_t i = 0; i < n_element->children->length; i++) {
    Node *node = (Node *) getFromList(n_element->children, i);
    domRepNode(node);
  }

  // WARNING: Not NULL-char-safe, so make sure tagName can't contain NULL chars
  if (!shr_void_tags_check(n_element->tagName->buffer)) {
    printf("</");
    printString(n_element->tagName);
    printf(">");
  }
}

void domRepDocument(Node *n_document) {
  for (uint32_t i = 0; i < n_document->children->length; i++) {
    Node *node = (Node *) getFromList(n_document->children, i);
    domRepNode(node);
  }
}

void domRepNode(Node *node) {
  switch (node->type) {
  case NT_DOCUMENT:
    domRepDocument(node);
    break;

  case NT_ELEMENT:
    domRepElement(node);
    break;

  case NT_TEXT:
    domRepText(node);
    break;

  case NT_DOCTYPE:
    domRepDoctype(node);
    break;

  case NT_COMMENT:
    domRepComment(node);
    break;
  }
}

#endif // _HDR_STRICTHTML_REPRESENT
