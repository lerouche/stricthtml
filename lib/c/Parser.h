#ifndef _HDR_STRICTHTML_PARSER
#define _HDR_STRICTHTML_PARSER

#include <unistd.h>

#include "./String.h"
#include "./Char.h"
#include "./Code.h"
#include "./DOM.h"
#include "./Print.h"

#include "../../rules/compiled/rules.h"

typedef enum {
  // Spaces between attribute name and value not allowed
  // Unquoted attribute values not allowed
  S_CONTENT, S_TAG_NAME, S_TAG_WHITESPACE, S_TAG_VALUE,
  S_START_TAG_END, S_END_TAG_SLASH,
  S_ATTR_NAME, S_ATTR_VALUE,
} ParseState;

const char *parsestate_e_strs[] = {
  "S_CONTENT",
  "S_TAG_NAME",
  "S_TAG_WHITESPACE",
  "S_TAG_VALUE",
  "S_START_TAG_END",
  "S_END_TAG_SLASH",
  "S_ATTR_NAME",
  "S_ATTR_VALUE",
};

typedef enum {
  TT_NONE, TT_STARTING, TT_ENDING, TT_DOCTYPE, TT_COMMENT,
} ParseTagType;

const char *parsetagtype_e_strs[] = {
  "TT_NONE",
  "TT_STARTING",
  "TT_ENDING",
  "TT_DOCTYPE",
  "TT_COMMENT",
};

typedef enum {
  // Make sure these line up with parsedatatype_map_tagnames
  D_JS, D_CSS, D_HTML
} ParseDataType;

const char *parsedatatype_e_strs[] = {
  "D_JS", "D_CSS", "D_HTML"
};

const char *parsedatatype_map_tagnames[] = {
  // Make sure these line up with parsedatatype_map_values
  "script",
  "style",
};

const ParseDataType parsedatatype_map_values[] = {
  D_JS,
  D_CSS,
};

const int parsedatatype_map_size = sizeof(parsedatatype_map_values) /
  sizeof(parsedatatype_map_values[0]);

typedef enum {
  SD_S_NONE,

  SD_S_JS_COMMENT_SL,
  SD_S_JS_COMMENT_ML,
  SD_S_JS_STRING_SQ,
  SD_S_JS_STRING_DQ,
  SD_S_JS_TEMPLATE,

  SD_S_CSS_COMMENT,
  SD_S_CSS_STRING_SQ,
  SD_S_CSS_STRING_DQ,
} ParseSpecialDataState;

const char *parsespecialdatastate_e_strs[] = {
  "SD_S_NONE",

  "SD_S_JS_COMMENT_SL",
  "SD_S_JS_COMMENT_ML",
  "SD_S_JS_STRING_SQ",
  "SD_S_JS_STRING_DQ",
  "SD_S_JS_TEMPLATE",

  "SD_S_CSS_COMMENT",
  "SD_S_CSS_STRING_SQ",
  "SD_S_CSS_STRING_DQ",
};

ParseDataType getDataType(String *tagName) {
  for (int i = 0; i < parsedatatype_map_size; i++) {
    if (equalCharArrays(tagName, parsedatatype_map_tagnames[i])) {
      return parsedatatype_map_values[i];
    }
  }

  return D_HTML;
}

typedef enum {
  NONE = 0,

  INVALID_TAG_NAME = 1,
  ENDING_TAG_HAS_WHITESPACE,
  UNMATCHED_ENDING_TAG,
  INVALID_TAG_CONTENT,
  INVALID_ATTR_NAME,
  UNQUOTED_ATTR_VALUE,
  EMPTY_ATTR_VALUE,
  ILLEGAL_VOID_ELEMENT_ENDING_TAG,
  EOF_UNCLOSED_TAG,

  UNTERMINATED_JS_STR = 1024,

  UNTERMINATED_CSS_STR = 2048,

  IIS_UNKNOWN_PARSESTATE = 4096,
  IIS_UNKNOWN_PARSEDATATYPE,
  IIS_UNKNOWN_PARSETAGTYPE_FOR_S_TAG_NAME,
  IIS_NO_CHEVRON_S_START_TAG_END,
  IIS_NO_SLASH_S_END_TAG_SLASH,
  IIS_NOT_D_HTML_S_END_TAG_SLASH,
} ParseError;

typedef struct {
  uint32_t pos;
  uint32_t posLine;
  uint32_t posCol;
  ParseError errorNo;
} Error;

Error *newError(uint32_t pos, uint32_t pos_line, uint32_t pos_col, ParseError error_no) {
  Error *error = calloc(1, sizeof(Error));
  error->pos = pos;
  error->posLine = pos_line;
  error->posCol = pos_col;
  error->errorNo = error_no;
  return error;
}

Error *parse(Code *code, Node **root_ref) {
  Node *root = newNode(0, 0, 0, NT_DOCUMENT);
  ParseState cur_parse_state = S_CONTENT;
  ParseTagType cur_parse_tag_type = TT_NONE;
  ParseDataType cur_parse_data_type = D_HTML;
  ParseSpecialDataState cur_parse_sd_state = SD_S_NONE;

  int sd_done = 0;

  int sd_js_lit_escape_next = 0;
  int sd_js_template_interpolate_depth = 0;
  int sd_js_template_interpolate_currdepth_brace_depth = 0;

  int sd_css_lit_escape_next = 0;

  ParseError errorNo;

  String *buffer = NULL;

  Node *cur_parent = root;
  Node *cur_text = NULL;
  Node *cur_value_tag = NULL;
  Attr *cur_attr = NULL;

  while (!codeAtEnd(code)) {
    char c = codeForward(code);
    char c2;

    debug(1, "Current state: %s\n",
      parsestate_e_strs[cur_parse_state]);
    debug(1, "Current tag type: %s\n",
      parsetagtype_e_strs[cur_parse_tag_type]);
    debug(1, "Current data type: %s\n",
      parsedatatype_e_strs[cur_parse_data_type]);
    debug(1, "Current special data state: %s\n",
      parsespecialdatastate_e_strs[cur_parse_sd_state]);
    debug(1, "Current char: (0x%0.2x) %s\n", c, char_rep_strs[((int) c) & 0x7f]);
    if (buffer != NULL) {
      debug(1, "buffer\n");
    }
    if (cur_text != NULL) {
      debug(1, "cur_text\n");
    }
    if (cur_value_tag != NULL) {
      debug(1, "cur_value_tag\n");
    }
    if (cur_attr != NULL) {
      debug(1, "cur_attr\n");
    }
    debug(1, "\n");

    switch (cur_parse_state) {
    case S_CONTENT:
      switch (cur_parse_data_type) {
      case D_HTML:
        if (c == '<') {
          // Is tag
          if (codePeek(code, 1) == '/') {
            // Ending tag
            cur_parse_state = S_END_TAG_SLASH;

          } else if (codePeek(code, 1) == '!') {
            // Comment or document type tag
            codeForward(code);
            if (codePeek(code, 1) == '-' && codePeek(code, 2) == '-') {
              codeForward(code);
              codeForward(code);
              cur_value_tag = newNode(code->nextPos - 1, code->curLine, code->curCol, NT_COMMENT);
              cur_parse_tag_type = TT_COMMENT;
            } else {
              cur_value_tag = newNode(code->nextPos - 1, code->curLine, code->curCol, NT_DOCTYPE);
              cur_parse_tag_type = TT_DOCTYPE;
            }
            buffer = newString();
            cur_value_tag->value = buffer;
            pushToList(cur_parent->children, cur_value_tag);
            cur_parse_state = S_TAG_VALUE;

          } else {
            // Starting tag
            buffer = newString();
            Node *new_element = newNode(code->nextPos - 1, code->curLine, code->curCol, NT_ELEMENT);
            new_element->tagName = buffer;
            new_element->parent = cur_parent;
            pushToList(cur_parent->children, new_element);
            cur_parent = new_element;
            cur_parse_tag_type = TT_STARTING;
            cur_parse_state = S_TAG_NAME;
          }

          cur_text = NULL;

        } else {
          // Is text
          if (cur_text == NULL) {
            cur_text = newNode(code->nextPos - 1, code->curLine, code->curCol, NT_TEXT);
            buffer = newString();
            cur_text->value = buffer;

            cur_text->parent = cur_parent;
            pushToList(cur_parent->children, cur_text);
          }

          appendToString(buffer, c);
        }
        break; // case D_HTML

      case D_JS:
        appendToString(buffer, c);
        switch (cur_parse_sd_state) {
        case SD_S_NONE:
          switch (c) {
          case '<':
            c2 = codePeek(code, 1);
            if (c2 == '/' && codeNextMatches(code, 1, parsedatatype_map_tagnames[cur_parse_data_type])) {
              // Closing tag </script>
              // sd_done will erase last '<' from buffer
              sd_done = 1;
            }
            break;

          case '/':
            c2 = codePeek(code, 1);
            if (c2 == '/' || c2 == '*') {
              appendToString(buffer, codeForward(code));
              cur_parse_sd_state = c2 == '/' ?
                SD_S_JS_COMMENT_SL :
                SD_S_JS_COMMENT_ML;
            }
            break; // case '/'

          case '"':
            cur_parse_sd_state = SD_S_JS_STRING_DQ;
            break; // case '"'

          case '\'':
            cur_parse_sd_state = SD_S_JS_STRING_SQ;
            break; // case '\''

          case '`':
            cur_parse_sd_state = SD_S_JS_TEMPLATE;
            break; // case '`'

          case '{':
            if (sd_js_template_interpolate_depth > 0) {
              sd_js_template_interpolate_currdepth_brace_depth++;
            }
            break; // case `{`

          case '}':
            if (sd_js_template_interpolate_depth > 0) {
              sd_js_template_interpolate_currdepth_brace_depth--;
              if (sd_js_template_interpolate_currdepth_brace_depth == -1) {
                sd_js_template_interpolate_depth--;
                sd_js_template_interpolate_currdepth_brace_depth = 0;
                cur_parse_sd_state = SD_S_JS_TEMPLATE;
              }
            }
            break;
          }
          break; // case SD_S_NONE

        case SD_S_JS_COMMENT_SL:
          if (c == '\r' || c == '\n') {
            cur_parse_sd_state = SD_S_NONE;
          }
          break; // case SD_S_JS_COMMENT_SL

        case SD_S_JS_COMMENT_ML:
          if (c == '*') {
            c2 = codePeek(code, 1);
            if (c2 == '/') {
              appendToString(buffer, codeForward(code));
              cur_parse_sd_state = SD_S_NONE;
            }
          }
          break; // case SD_S_JS_COMMENT_ML

        case SD_S_JS_STRING_SQ:
        case SD_S_JS_STRING_DQ:
        case SD_S_JS_TEMPLATE:
          switch (c) {
          case '\\':
            // WARNING: Should handle multi-char escapes, but untested
            sd_js_lit_escape_next =
              sd_js_lit_escape_next ^ 1;
            break; // case '\\'

          case '$':
            if (cur_parse_sd_state == SD_S_JS_TEMPLATE &&
            sd_js_lit_escape_next == 0) {
              c2 = codePeek(code, 1);
              if (c2 == '{') {
                appendToString(buffer, codeForward(code));
                sd_js_template_interpolate_depth++;
                cur_parse_sd_state = SD_S_NONE;
              }
            }
            sd_js_lit_escape_next = 0;
            break; // case '$'

          case '\r':
          case '\n':
            if (sd_js_lit_escape_next) {
              // NOTE: Escaping line terminators in templates does
              // remove them
              c2 = codePeek(code, 1);
              if (c == '\r' && c2 == '\n') {
                appendToString(buffer, codeForward(code));
              }
            } else {
              if (cur_parse_sd_state != SD_S_JS_TEMPLATE) {
                errorNo = UNTERMINATED_JS_STR;
                goto error;
              }
            }
            sd_js_lit_escape_next = 0;
            break; // case '\r' AND '\n'

          case '\'':
          case '"':
          case '`':
            if (((cur_parse_sd_state == SD_S_JS_STRING_SQ &&
            c == '\'') || (cur_parse_sd_state == SD_S_JS_STRING_DQ &&
            c == '"') || (cur_parse_sd_state == SD_S_JS_TEMPLATE &&
            c == '`')) && !sd_js_lit_escape_next) {
              cur_parse_sd_state = SD_S_NONE;
            }
            sd_js_lit_escape_next = 0;
            break; // case '\'' AND '"' AND '`'

          default:
            sd_js_lit_escape_next = 0;
          }
          break; // case SD_S_JS_STRING_SQ AND SD_S_JS_STRING_DQ AND SD_S_JS_TEMPLATE

        default:
          break;
        } // switch (cur_parse_sd_state)
        break; // case D_JS

      case D_CSS:
        appendToString(buffer, c);
        switch (cur_parse_sd_state) {
        case SD_S_NONE:
          switch (c) {
          case '<':
            c2 = codePeek(code, 1);
            if (c2 == '/' && codeNextMatches(code, 1, parsedatatype_map_tagnames[cur_parse_data_type])) {
              // Closing tag </style>
              // sd_done will erase last '<' from buffer
              sd_done = 1;
            }
            break;

          case '/':
            c2 = codePeek(code, 1);
            if (c2 == '*') {
              appendToString(buffer, codeForward(code));
              cur_parse_sd_state = SD_S_CSS_COMMENT;
            }
            break; // case '/'
          } // switch (c)
          break; // case SD_S_NONE

        case SD_S_CSS_COMMENT:
          if (c == '*') {
            c2 = codePeek(code, 1);
            if (c2 == '/') {
              appendToString(buffer, codeForward(code));
              cur_parse_sd_state = SD_S_NONE;
            }
          }
          break; // case SD_S_CSS_COMMENT

        case SD_S_CSS_STRING_SQ:
        case SD_S_CSS_STRING_DQ:
          switch (c) {
          case '\\':
            // WARNING: Should handle multi-char escapes, but untested
            sd_css_lit_escape_next =
              sd_css_lit_escape_next ^ 1;
            break; // case '\\'

          case '\r':
          case '\n':
            if (sd_css_lit_escape_next) {
              c2 = codePeek(code, 1);
              if (c == '\r' && c2 == '\n') {
                appendToString(buffer, codeForward(code));
              }
            } else {
              errorNo = UNTERMINATED_CSS_STR;
              goto error;
            }
            sd_css_lit_escape_next = 0;
            break; // case '\r' AND '\n'

          case '\'':
          case '"':
            if (((cur_parse_sd_state == SD_S_CSS_STRING_SQ &&
            c == '\'') || (cur_parse_sd_state == SD_S_CSS_STRING_DQ &&
            c == '"')) && !sd_css_lit_escape_next) {
              cur_parse_sd_state = SD_S_NONE;
            }
            sd_css_lit_escape_next = 0;
            break; // case '"' AND '`'

          default:
            sd_css_lit_escape_next = 0;
          }
          break; // case SD_S_CSS_STRING_SQ AND SD_S_CSS_STRING_DQ

          default:
            break;
        } // switch (cur_parse_sd_state)
        break; // case D_CSS

      default:
        errorNo = IIS_UNKNOWN_PARSEDATATYPE;
        goto error;
      } // switch (cur_parse_data_type)

      if (sd_done) {
        removeLastCharFromString(buffer);
        cur_parse_data_type = D_HTML;
        cur_parse_state = S_END_TAG_SLASH;
        cur_text = NULL;
        sd_done = 0;
      }
      break; // case S_CONTENT

    case S_TAG_NAME:
      if (!cIsLcAlpha(c) && c != '-' && !cIsDigit(c)) {
        errorNo = INVALID_TAG_NAME;
        goto error;
      }
      appendToString(buffer, c);

      c2 = codePeek(code, 1);
      if (cIsWhitespace(c2) || c2 == '>') {
        // <c> is last character of tag name
        // <buffer> can't be empty due to above <appendToString>

        switch (cur_parse_tag_type) {
        case TT_ENDING:
          // Ending tag, so chevron should come immediately after
          if (c2 != '>') {
            errorNo = ENDING_TAG_HAS_WHITESPACE;
            goto error;
          }
          codeForward(code);

          // WARNING: Not NULL-char-safe, so make sure tagName can't contain NULL chars
          if (shr_void_tags_check(buffer->buffer)) {
            errorNo = ILLEGAL_VOID_ELEMENT_ENDING_TAG;
            goto error;
          }

          // Find which ancestor this closes
          while (cur_parent != root && equalStrings(
          cur_parent->tagName, buffer) == 0) {
            cur_parent = cur_parent->parent;
          }
          if (cur_parent == root) {
            errorNo = UNMATCHED_ENDING_TAG;
            goto error;
          }
          cur_parent = cur_parent->parent;
          cur_parse_tag_type = TT_NONE;
          cur_parse_state = S_CONTENT;
          break; // case TT_ENDING

        case TT_STARTING:
          // Starting tag
          cur_parse_state = c2 == '>' ?
            S_START_TAG_END :
            S_TAG_WHITESPACE;
          break; // TT_STARTING:

        default:
          errorNo = IIS_UNKNOWN_PARSETAGTYPE_FOR_S_TAG_NAME;
          goto error;
        }

        buffer = NULL;
      }
      break; // case S_TAG_NAME

    case S_TAG_VALUE:
      if ((cur_parse_tag_type == TT_COMMENT &&
      c == '-' && codePeek(code, 1) == '-' &&
      codePeek(code, 2) == '>') ||
          (cur_parse_tag_type == TT_DOCTYPE &&
      c == '>')) {
        cur_value_tag = NULL;
        cur_parse_tag_type = TT_NONE;
        cur_parse_state = S_CONTENT;
      } else {
        appendToString(buffer, c);
      }
      // WARNING: No illegal state check
      break; // case S_TAG_VALUE

    case S_TAG_WHITESPACE:
      if (!cIsWhitespace(c)) {
        errorNo = INVALID_TAG_CONTENT;
        goto error;
      }

      c2 = codePeek(code, 1);
      if (c2 == '>') {
        // Next char is end of starting tag (ending tags
        // do not go into S_TAG_WHITESPACE state)
        cur_parse_state = S_START_TAG_END;
      } else if (cIsLcAlpha(c2)) {
        // Next char is start of attribute
        cur_attr = newAttr(code->nextPos, code->curLine, code->curCol + 1);
        pushToList(cur_parent->attrs, cur_attr);
        buffer = newString();
        cur_attr->name = buffer;
        cur_parse_state = S_ATTR_NAME;
      }
      break; // case S_TAG_WHITESPACE

    case S_START_TAG_END:
      if (c != '>') {
        errorNo = IIS_NO_CHEVRON_S_START_TAG_END;
        goto error;
      }
      // WARNING: Not NULL-char-safe, so make sure tagName can't contain NULL chars
      if (shr_void_tags_check(cur_parent->tagName->buffer)) {
        cur_parent = cur_parent->parent;
      } else {
        cur_parse_data_type = getDataType(cur_parent->tagName);
        if (cur_parse_data_type != D_HTML) {
          cur_text = newNode(code->nextPos, code->curLine, code->curCol + 1, NT_TEXT);
          pushToList(cur_parent->children, cur_text);
          buffer = newString();
          cur_text->value = buffer;
        }
      }
      cur_parse_tag_type = TT_NONE;
      cur_parse_state = S_CONTENT;
      break; // case S_START_TAG_END

    case S_END_TAG_SLASH:
      if (cur_parse_data_type != D_HTML) {
        errorNo = IIS_NOT_D_HTML_S_END_TAG_SLASH;
        goto error;
      }
      if (c != '/') {
        errorNo = IIS_NO_SLASH_S_END_TAG_SLASH;
        goto error;
      }
      cur_parse_tag_type = TT_ENDING;
      buffer = newString();
      cur_parse_state = S_TAG_NAME;
      break; // case S_END_TAG_SLASH

    case S_ATTR_NAME:
      if (!cIsLcAlpha(c) && c != '-') {
        errorNo = INVALID_ATTR_NAME;
        goto error;
      }
      appendToString(buffer, c);

      c2 = codePeek(code, 1);
      if (c2 == '=') {
        if (codePeek(code, 2) != '"') {
          errorNo = UNQUOTED_ATTR_VALUE;
          goto error;
        }
        codeForward(code);
        codeForward(code);

        buffer = newString();
        cur_attr->value = buffer;
        cur_parse_state = S_ATTR_VALUE;
      } else if (cIsWhitespace(c2) || c2 == '>') {
        buffer = NULL;
        cur_attr = NULL;
        cur_parse_state = c2 == '>' ?
          S_START_TAG_END :
          S_TAG_WHITESPACE;
      }
      break; // case S_ATTR_NAME

    case S_ATTR_VALUE:
      if (c == '"') {
        if (buffer->length == 0) {
          errorNo = EMPTY_ATTR_VALUE;
          goto error;
        }
        buffer = NULL;
        cur_attr = NULL;

        c2 = codePeek(code, 1);
        cur_parse_state = c2 == '>' ?
          S_START_TAG_END :
          S_TAG_WHITESPACE; // S_TAG_WHITESPACE will return error if not either

      } else {
        appendToString(buffer, c);
      }
      break; // case S_ATTR_VALUE

    default:
      errorNo = IIS_UNKNOWN_PARSESTATE;
      goto error;
    } // switch (cur_parse_state)
  }

  if (cur_parse_state != S_CONTENT) {
    errorNo = EOF_UNCLOSED_TAG;
    goto error;
  }

  debug(1, "Done!\n");
  debug(1, "\n");

  *root_ref = root;
  return NULL;

error:
  deleteNode(root);
  // WARNING: If nextPos is 0, argument will underflow
  // (this shouldn't happen if codeForward is called at least once before goto error)
  return newError(code->nextPos - 1, code->curLine, code->curCol, errorNo);
}

#endif // _HDR_STRICTHTML_PARSER
