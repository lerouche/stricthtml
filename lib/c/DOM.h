#ifndef _HDR_STRICTHTML_DOM
#define _HDR_STRICTHTML_DOM

#include "./String.h"
#include "./List.h"

typedef enum nodetype_e NodeType;
typedef struct node_st Node;
typedef struct attr_st Attr;

enum nodetype_e {
  NT_DOCUMENT, NT_ELEMENT, NT_TEXT, NT_DOCTYPE, NT_COMMENT
};

const char *nodetype_e_strs[] = {
  "NT_DOCUMENT",
  "NT_ELEMENT",
  "NT_TEXT",
  "NT_DOCTYPE",
  "NT_COMMENT"
};

struct node_st {
  Node *parent;

  NodeType type;
  // Only for NT_ELEMENT
  String *tagName;
  // Only for NT_TEXT, NT_DOCTYPE, NT_COMMENT
  String *value;

  // Only for NT_ELEMENT
  List *attrs;
  // Only for NT_DOCUMENT, NT_ELEMENT
  List *children;

  uint32_t pos;
  uint32_t posLine;
  uint32_t posCol;
};

struct attr_st {
  String *name;
  String *value;

  uint32_t pos;
  uint32_t posLine;
  uint32_t posCol;
};

Node *newNode(uint32_t pos, uint32_t pos_line, uint32_t pos_col, NodeType type) {
  Node *node = calloc(1, sizeof(Node));
  node->type = type;
  if (type == NT_ELEMENT) {
    node->attrs = newList();
  }
  if (type == NT_DOCUMENT || type == NT_ELEMENT) {
    node->children = newList();
  }
  node->pos = pos;
  node->posLine = pos_line;
  node->posCol = pos_col;
  return node;
}

Attr *newAttr(uint32_t pos, uint32_t pos_line, uint32_t pos_col) {
  Attr *attr = calloc(1, sizeof(Attr));
  attr->pos = pos;
  attr->posLine = pos_line;
  attr->posCol = pos_col;
  return attr;
}

void deleteAttr(Attr *attr) {
  deleteString(attr->name);
  deleteString(attr->value);
  free(attr);
}

void deleteNode(Node *node) {
  if (node->tagName != NULL) {
    deleteString(node->tagName);
  }
  if (node->value != NULL) {
    deleteString(node->value);
  }
  if (node->attrs != NULL) {
    for (uint32_t i = 0; i < node->attrs->length; i++) {
      deleteAttr((Attr *) getFromList(node->attrs, i));
    }
    deleteList(node->attrs);
  }
  if (node->children != NULL) {
    for (uint32_t i = 0; i < node->children->length; i++) {
      deleteNode((Node *) getFromList(node->children, i));
    }
    deleteList(node->children);
  }
  free(node);
}

#endif // _HDR_STRICTHTML_DOM
