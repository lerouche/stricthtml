#!/usr/bin/env bash

pushd "$(dirname "$0")" > /dev/null

mkdir -p compiled

void_elems=()
readarray -t void_elems < void.txt

for compiler in compile/*.sh; do
  basefile=$(basename "$compiler")
  base=${basefile%.*}

  source "$compiler"
  gen_void_rules void_elems[@] > "compiled/rules.$base"
done

popd > /dev/null

exit 0
