#!/usr/bin/env bash

gen_void_rules() {
  declare -a tags=("${!1}")

  echo "\
var shr_void_tags = Object.create(null);
(function() {"

  for tag in "${tags[@]}"; do
    echo "\
  shr_void_tags[\"$tag\"] = true;"
  done

  echo "\
})();

function shr_void_tags_check(tag) {
  return !!shr_void_tags[tag];
}"
}
