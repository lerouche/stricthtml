#!/usr/bin/env bash

gen_void_rules() {
  declare -a tags=("${!1}")

  echo "\
#ifndef _HDR_STRICTHTML_RULES
#define _HDR_STRICTHTML_RULES

#include \"../../lib/c/khash.h\"

KHASH_SET_INIT_STR(shr_void_tags)

static khash_t(shr_void_tags) *shr_void_tags_h;

static void shr_void_tags_init() {
  shr_void_tags_h = kh_init(shr_void_tags);
  "

  for tag in "${tags[@]}"; do
    echo "\
  int rv_$tag;
  kh_put(shr_void_tags, shr_void_tags_h, \"$tag\", &rv_$tag);"
  done

  echo "\
}

int shr_void_tags_check(char *tag) {
  khint_t k = kh_get(shr_void_tags, shr_void_tags_h, tag);
  return k != kh_end(shr_void_tags_h);
}

void shr_init() {
  shr_void_tags_init();
}

#endif // _HDR_STRICTHTML_RULES"
}
