#!/usr/bin/env node

"use strict";

const Minimist = require("minimist");
const UglifyJS = require("uglify-js");
const FileSystem = require("fs");

const DEBUG = Minimist(process.argv.slice(2)).debug;
const COMPILED_PATH = __dirname + "/out/stricthtml_em.js";

let code = FileSystem.readFileSync(COMPILED_PATH, "utf8");

code = code.replace(
  "var data=Module[\"readBinary\"](memoryInitializer)",
  "var data=Module[\"readBinary\"](__dirname + '/' + memoryInitializer)"
);

code = `(function(undefined){${code}})()`;

if (!DEBUG) {
  let compressSettings = {
    booleans: true,
    collapse_vars: true,
    comparisons: true,
    conditionals: true,
    dead_code: true,
    drop_console: true,
    drop_debugger: true,
    evaluate: true,
    hoist_funs: true,
    hoist_vars: false,
    if_return: true,
    join_vars: true,
    keep_fargs: true,
    keep_fnames: false,
    loops: true,
    negate_iife: true,
    properties: true,
    reduce_vars: true,
    sequences: true,
    unsafe: true,
    unused: true,
    warnings: true,
  };

  code = UglifyJS.minify(code, {
    warnings: true,
    mangle: {
      eval: true,
    },
    compress: compressSettings,
  }).code;
}

FileSystem.writeFileSync(COMPILED_PATH, code);
