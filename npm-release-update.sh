#!/usr/bin/env bash

set -e

pushd "$(dirname "$0")" > /dev/null

./compile.sh em
npm --no-git-tag-version version $1
npm publish

popd > /dev/null

exit 0
