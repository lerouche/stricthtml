#include "../../lib/c/Parser.h"
#include <emscripten.h>

EMSCRIPTEN_KEEPALIVE
void em_init() {
  shr_init();
}

// Make NULL-char-safe: pass length as well
EMSCRIPTEN_KEEPALIVE
void em_entry(char *code_char_array, uint32_t code_char_array_len, void (*callback)(void*, void *)) {
  String *code_str = stringFromCharArray(code_char_array_len, code_char_array, code_char_array_len);
  Code *code = newCode(code_str);

  Node *root;
  Error *err = parse(code, &root);

  if (err != NULL) {
    callback(err, NULL);
  } else {
    callback(NULL, root);
  }
}
