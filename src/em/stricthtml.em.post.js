(function (Module, undefined) {
  _em_init();

  var cur_callback;
  var p_cur_code;

  var ENUM_DATA_BYTES = 4;
  var ENUM_DATA_TYPE = "i32";
  var PTR_DATA_BYTES = 4;
  var PTR_DATA_TYPE = "*";

  function build_parse_error (pos, line, column, errNo) {
    var err = new SyntaxError("An error " + errNo + " occurred at line " + line + ", column " + column);
    err.shPos = line;
    err.shLine = line;
    err.shColumn = column;
    err.shErrNo = errNo;
    return err;
  }

  var NODE_TYPE = [
    "NT_DOCUMENT",
    "NT_ELEMENT",
    "NT_TEXT",
    "NT_DOCTYPE",
    "NT_COMMENT",
  ];

  function read_and_free_String (p_String) {
    var addr = p_String;

    var p_buffer = getValue(addr, PTR_DATA_TYPE);
    addr += PTR_DATA_BYTES;

    var length = getValue(addr, "i32");

    var js_str = Pointer_stringify(p_buffer, length);

    _free(buffer);
    _free(p_String);

    return js_str;
  }

  function read_and_free_List (p_List) {
    // WARNING: Does not free data that pointers in the list point to
    var addr = p_List;

    var p_buffer = getValue(addr, PTR_DATA_TYPE);
    addr += PTR_DATA_BYTES;

    var length = getValue(addr, "i32");

    var subaddr = p_buffer;
    var js_arr = [];
    for (var i = 0; i < length; i++) {
      var p_obj = getValue(subaddr, PTR_DATA_TYPE);
      subaddr += PTR_DATA_BYTES;
      js_arr.push(p_obj);
    }

    _free(buffer);
    _free(p_List);

    return js_arr;
  }

  function parser_callback (p_err, p_document) {
    _free(p_cur_code);

    if (p_err !== 0) {
      var err_pos = getValue(p_err, "i32");
      var err_lineNo = getValue(p_err + 4, "i32");
      var err_colNo = getValue(p_err + 8, "i32");
      var err_errorNo = getValue(p_err + 12, ENUM_DATA_TYPE);

      _free(p_err);

      cur_callback(build_parse_error(err_pos, err_lineNo, err_colNo, err_errorNo));
      return;
    }

    var root;

    var to_process = [[p_document, null]];
    var _;

    while (_ = to_process.shift()) {
      var p_node = _[0];
      var parent = _[1];
      var obj = {};
      obj.parent = _[1];

      // Skip *parent
      var addr = p_node + PTR_DATA_BYTES;

      var type = NODE_TYPE[getValue(addr, ENUM_DATA_TYPE)];
      addr += ENUM_DATA_BYTES;
      obj.type = type;

      var p_tag_name = getValue(addr, PTR_DATA_TYPE);
      addr += PTR_DATA_BYTES;
      if (p_tag_name) {
        obj.tagName = read_and_free_String(p_tag_name);
      }

      var p_value = getValue(addr, PTR_DATA_TYPE);
      addr += PTR_DATA_BYTES;
      if (p_value) {
        obj.value = read_and_free_String(p_value);
      }

      var p_attrs = getValue(addr, PTR_DATA_TYPE);
      addr += PTR_DATA_BYTES;
      if (p_attrs) {
        obj.attrs = read_and_free_List(p_attrs)
          .map(function (p_attr) {
            var addr = p_attr;

            var p_name = getValue(addr, PTR_DATA_TYPE);
            addr += PTR_DATA_BYTES;
            var name = read_and_free_String(p_name);

            var p_value = getValue(addr, PTR_DATA_TYPE);
            addr += PTR_DATA_BYTES;
            var value = read_and_free_String(p_value);

            var pos = getValue(addr, "i32");
            addr += 4;

            var pos_line = getValue(addr, "i32");
            addr += 4;

            var pos_col = getValue(addr, "i32");
            addr += 4;

            _free(p_attr);

            return {
              name: name,
              value: value,
              pos: pos,
              posLine: pos_line,
              posCol: pos_col,
            };
          });
      }

      var p_children = getValue(addr, PTR_DATA_TYPE);
      addr += PTR_DATA_BYTES;
      if (p_children) {
        obj.children = [];
        read_and_free_List(p_children)
          .forEach(function (p_child) {
            to_process.push([p_child, obj]);
          });
      }

      obj.pos = getValue(addr, "i32");
      addr += 4;

      obj.posLine = getValue(addr, "i32");
      addr += 4;

      obj.posCol = getValue(addr, "i32");
      addr += 4;

      _free(p_node);

      if (parent) {
        parent.children.push(obj);
      } else {
        root = obj;
      }
    }

    cur_callback(null, root);
  }

  var p_parser_callback = addFunction(parser_callback, "vii");

  Module.parse = function (code, callback) {
    cur_callback = callback;

    var code_as_int_array = intArrayFromString(code);

    p_cur_code = allocate(code_as_int_array, "i8", ALLOC_NORMAL);
    // code_as_int_array is NULL-terminated, so remove one from length
    _em_entry(p_cur_code, code_as_int_array.length - 1, p_parser_callback);
  };

  function stringify_Comment (n_comment) {
    return "<!--" + n_comment.value + "-->";
  }

  function stringify_Doctype (n_doctype) {
    return "<!" + n_doctype.value + ">";
  }

  function stringify_Text (n_text) {
    return n_text.value;
  }

  function stringify_Element (n_element) {
    var html = "<" + n_element.tagName;
    n_element.attrs.forEach(function (attr) {
      html += " " + attr.name + "=\"" + attr.value + "\"";
    });
    html += ">";
    n_element.children.forEach(function (child) {
      html += stringify_Node(child);
    });
    if (!shr_void_tags_check(n_element.tagName)) {
      html += "</" + n_element.tagName + ">";
    }
    return html;
  }

  function stringify_Document (n_document) {
    return n_document.children.map(function (child) {
      return stringify_Node(child);
    })
      .join("");
  }

  function stringify_Node (node) {
    switch (node.type) {
    case "NT_DOCUMENT":
      return stringify_Document(node);

    case "NT_ELEMENT":
      return stringify_Element(node);

    case "NT_TEXT":
      return stringify_Text(node);

    case "NT_DOCTYPE":
      return stringify_Doctype(node);

    case "NT_COMMENT":
      return stringify_Comment(node);
    }
  }

  Module.stringify = stringify_Node;
})(Module);
