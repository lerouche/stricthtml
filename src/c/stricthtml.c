#include <stdio.h>

#include "../../lib/c/Parser.h"
#include "../../lib/c/Represent.h"

int main(int argc, char **argv) {
  shr_init();

  Node *root;
  String *code_str = newString();
  char c;
  while (read(0, &c, 1) > 0) {
    appendToString(code_str, c);
  }
  Code *code = newCode(code_str);
  Error *err = parse(code, &root);
  if (err != NULL) {
    fprintf(stderr, "[ERR] %d on line %d, column %d\n", err->errorNo, err->posLine, err->posCol);
    return err->errorNo;
  }

  domRepDocument(root);

  return 0;
}
